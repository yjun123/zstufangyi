import requests
from log import logger
import json

class BusinessForm_struct():
    def __init__(self):
        pass

    def keys(self):
        return {''}

class FormInfo_struct():
    Sys_CompanyId = None
    Sys_CompanyName = None
    Sys_UserPhone = ""
    Sys_UserAvatar = ""
    def __init__(self):
        pass

    def keys(self):
        return (
            'Sys_UserId', 'Sys_UserName', 'Sys_UserAccount',
            'Sys_CompanyId', 'Sys_CompanyName', 'Sys_DepartmentId',
            'Sys_DepartmentName', 'Sys_UserEmail', 'Sys_UserPhone',
            'Sys_JobName', 'Sys_ApplyDate', 'Sys_OrgPath',
            'Sys_ApplyNo', 'Sys_UserAvatar'
        )
    
    def __getitem__(self, item):
        return getattr(self, item)

class Api():
    def __init__(
        self,
        token: str,
        host: str = 'fangyi.zstu.edu.cn',
        port: int = 8008,
        timeout: int = 10
    ):
        self.token = token
        self.host = host
        self.port = port
        self.timeout = timeout
        self.client = requests.Session()

    def GetDataSourceByNo(
        self,
        sqlNo: str
    ):
        """ SQL 数据库查询接口
        sqlNo: SQL 指令
        """
        headers = {
            'Authorization': f'Bearer {self.token}',
        }

        return self.get(
            'form/api/DataSource/GetDataSourceByNo',
            params= {
                'sqlNo': sqlNo
            },
            headers=headers
        )

    def SubmitBusinessForm(
        self,
        BusinessForm_struct: dict
    ):
        """  提交今日健康打卡的数据
        BusinessForm_struct：数据
        """
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}',
        }

        return self.post(
            'form/api/FormHandler/SubmitBusinessForm',
            payload= BusinessForm_struct,
            headers= headers
        )
    
    def GetFormInfo(
        self,
        FormInfo_struct: dict,
        formId: str = '1817056F47E744D3B8488B',
        bizId: int = None
    ):
        """  获取今日健康打卡的表格内容
        FormInfo_struct: 数据
        formId: 打卡页面的Form ID, 固定为 1817056F47E744D3B8488B
        bizId:  (Business ID) 未知, 默认为空
        """
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}',
        }

        return self.post(
            'form/api/FormHandler/GetFormInfo',
            params= {
                'formId': formId,
                'bizId': bizId
            },
            payload= FormInfo_struct,
            headers= headers
            # payload= dict(FormInfo_struct)
        )
    
    def GetFileByte(
        self,
        t: int,
        id: str = '1817056F47E744D3B8488B',
        jsType: int = 2
    ):
        """ 获取健康打卡页面的js代码
        t： 请求时的时间戳
        id: 打卡页面的Form ID, 固定为 1817056F47E744D3B8488B
        jsType: 未知，默认为2
        """
        return self.get(
            'form/api/FormExtend/GetFileByte',
            params= {
                'id': id,
                't': t,
                'jsType': jsType
            }            
        )
    
    def GetLocation(
        self,
        latitude: float,
        longitude: float
    ):
        """ 根据经纬度请求详细地址
        latitude: 纬度
        longitude：经度
        """
        return self.get(
            'form/api/HealthRecords/GetLocation',
            params= {
                'latitude': latitude,
                'longitude': longitude
            }
        )

    #######################
    def baseRequest(
        self,
        method: str,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None
    ) -> dict:
        """基础请求方法"""

        try:
            resp = self.client.request(
                method,
                f'http://{self.host}:{self.port}/{path}',
                headers= headers,
                params= params,
                data = json.dumps(payload),
                timeout= self.timeout
            )
            resp.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if isinstance(e, requests.exceptions.Timeout):
                logger.warning(f'响应超时! => {e}')
            elif isinstance(e, requests.exceptions.HTTPError):
                logger.error(f'响应码出错 => {resp.status_code}')
            else:
                logger.error(f'请求出错')
            
            print(resp.text)
            return {}

        # 处理数据
        try:
            data = resp.json()
        except json.JSONDecodeError as e:
            logger.error('响应结果非json格式')
            print(resp.text)
            return {}
        
        if data is None:
            logger.error('返回为 NULL')
            return {}

        return data

    def post(
        self,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None,
    ):
        return self.baseRequest(
            'POST',
            path,
            params= params, payload= payload, headers= headers
        )  

    def get(
        self,
        path: str,
        params: dict = None,
        headers: dict = None,
        payload: dict = None,
    ):
        return self.baseRequest(
            'GET',
            path,
            params= params, headers= headers, payload= payload
        )

import sys
if __name__ == '__main__':
    logger.info(f"{sys.argv[0]}: 接口")