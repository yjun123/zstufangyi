#!/usr/bin/python3
# 账户文件 添加新帐号脚本
# author: yjun
# date: 2021/03/07

import json
import sys
import getopt


CONFIG_FILE_PATH='./config/accounts.json'
CONFIG_FILE_BACKUP_PATH='./config/accounts-bak.json'

def help():
    print('''usage: \n\tadd_account.py -u <username> -n <name>

1. 添加账号: python3 add_account.py -u 2024330100001 -n 张三

2. 根据学号删除账号: python3 add_account.py -u 2024330100001 

3. 列出当前的账号: python3 add_account.py -l

4. 暂停某个账号的打卡: python3 add_account.py -c -u 2024330100001

5. 恢复暂停的某个账号: python3 add_account.py -e -u 2024330100001

''')

def read_json(db):
    data = None

    try:
        with open(db) as f:
            data = json.load(f)
    except FileNotFoundError:
        return {}
        
    return data

def save_json(db, data):
    if data:
        with open(db, 'w+', encoding='utf-8') as f:
            json.dump(data, f, indent=4, separators=(',', ':'),ensure_ascii=False)

def list_accounts():
    # 读取本地的账户文件
    accounts = read_json(CONFIG_FILE_PATH)
    new_accounts = accounts
    print("UserID                 State           Name")
    for account in new_accounts:
        print(f"{account['username']}           {str(account['active']):<5}           {account['name']}")

def save_accounts(accounts, new_accounts):
    # 备份原账户文件
    save_json(data=accounts, db=CONFIG_FILE_BACKUP_PATH)
    # 保存新的账户文件
    save_json(data=new_accounts, db=CONFIG_FILE_PATH)            

def delete_account(username):
    # 读取本地的账户文件
    accounts = read_json(CONFIG_FILE_PATH)
    new_accounts = accounts
    for account in new_accounts:
        if account['username'] == username:
            new_accounts.remove(account)
            print(f'account {username} delete successfully!')
            save_accounts(accounts=accounts, new_accounts=new_accounts)
            return 0

    print(f'account {username} NOT FOUND!')

def disable_account(username):
    # 读取本地的账户文件
    accounts = read_json(CONFIG_FILE_PATH)
    new_accounts = accounts
    for account in new_accounts:
        if account['username'] == username or username == 'all':
            # new_accounts.remove(account)
            new_accounts[new_accounts.index(account)]['active'] = False
            print(f"account {account['username']} disable successfully!")
            save_accounts(accounts=accounts, new_accounts=new_accounts)
            if username != 'all':
                return 0
    if username == 'all':
        return 0

    print(f'account {username} NOT FOUND!')
    sys.exit(-4)

def enable_account(username):
    # 读取本地的账户文件
    accounts = read_json(CONFIG_FILE_PATH)
    new_accounts = accounts
    for account in new_accounts:
        if account['username'] == username or username == 'all':
            # new_accounts.remove(account)
            new_accounts[new_accounts.index(account)]['active'] = True
            print(f"account {account['username']} enable successfully!")
            save_accounts(accounts=accounts, new_accounts=new_accounts)
            if username != 'all':
                return 0
    if username == 'all':
        return 0

    print(f'account {username} NOT FOUND!')
    sys.exit(-4)

def add_account(username, name):
    # 读取本地的账户文件
    accounts = read_json(CONFIG_FILE_PATH)

    print('get username: ', username)
    print('get name: ', name)

    active = True

    new_accounts = list(accounts)
    new_accounts.append({'username' : username, 'name': name, 'active': active})
    print(f'add {username} to account db successfully!')
    
    save_accounts(accounts=accounts, new_accounts=new_accounts)

def check_today(username):
    F = Fangyi()

    today_data = F.GetTodayCheckinData(username)['data']

    return today_data

def main(argv):
    username = None
    password = None
    name = None
    delete_flag = False
    cancel_flag = False
    enable_flag = False
    all_flag = False
    update_flag = False
    list_flag = False

    try:
        opts, args = getopt.getopt(argv, "n:u:dceyl", ["name=","username=","delete", "cancel", "enable", "update", "list"])
    except getopt.GetoptError:
        help()
        sys.exit(-2)
    
    for opt, arg in opts:
        if opt == '-h':
            help()
            sys.exit(0)
        elif opt in ("-u", "--username"):
            username = arg
        elif opt in ("-n", "--name"):
            name = arg
        elif opt in ("-d", "--delete"):
            delete_flag = True
        elif opt in ("-c", "--cancel"):
            cancel_flag = True
        elif opt in ("-e" , "--enable"):
            enable_flag = True
        elif opt in ("-l", "--list"):
            list_flag = True

    if list_flag:
        list_accounts()
        sys.exit(0)

    if delete_flag and username:
        delete_account(username)
        sys.exit(0)
    
    if cancel_flag and username:
        disable_account(username)
        sys.exit(0)

    if enable_flag and username:
        enable_account(username)
        sys.exit(0)

    if not username or not name:
        help()
        sys.exit(-3)
    else:
        add_account(username=username, name=name)
        if not check_today(username= username):
            select = input(f"检测到 学号 {username} 姓名 {name} 今日未打卡. 是否打卡? Y/N:")
            if select in ['Y', 'y', 'Yes', 'yes']:
                CheckinOne(username, name)

if __name__ == '__main__':
    from ZstuFangyi import CheckinOne, Fangyi
    main(sys.argv[1:])
