# 浙江理工大学防疫打卡

 2021年12月9号， 浙江理工大学将 e浙理的 健康打卡签到系统 迁移到了 <http://fangyi.zstu.edu.cn:6006>

*身份授权认证* 使用 [IdentityServer4](http://fangyi.zstu.edu.cn:4500/) 实现

## 前置要求

- Python3环境
- Linux环境 （Linux 服务器/树莓派）

## 脚本依赖

`requests`

## 脚本使用说明

1. 添加授权登录信息

打开 [统一身份认证平台](https://sso.zstu.edu.cn/login)，输入帐号密码；

进入审查元素（按下F12或者设置里找），切换标签页到网络(Network)；

 ![审查元素](images/sso-inspect.png) 

登录帐号，这时网络标签页会出现网络请求的数据；

![sso-login](images/sso-login.png)

点击第一个数据项(login), 右边弹出这个请求的相关数据，点击payload；

将 `username`，`type`，`execution`，`croypto`，`password` 对应的值复制填入到 `config/authorized_data.json`中；

2. 添加需要打卡的学号

   `python3 add_account.py -u 学号 -n 昵称`

   ```
   $ python3 add_account.py -u 2024123412345 -n 张三                                                                               
   get username:  2024123412345
   get name:  张三
   add 2024123412345 to account db successfully!
   ```

3. 执行打卡脚本

   `python3 ZstuFangyi.py --submit`

4. 定时自动执行

   为了减少不必要的代码（懒，这个脚本并没有定时执行功能，而依赖于具体平台的实现，这里我以Linux下的 [Systemd Timer](http://www.jinbuguo.com/systemd/systemd.timer.html) 为例说明;

   - 将 systemd目录 下的 `ZstuFangyiAuto.service`和`ZstuFangyiAuto.timer`拷贝到 `$HOME/.config/systemd/user`目录下;

   - 修改路径

     将 `ZstuFangyiAuto.service`内的`ExecStart`和`WorkingDirectory`修改成当前脚本所在的目录;

     ```
     ExecStart=python3 path/to/the/script/ZstuFangyi.py --submit
     WorkingDirectory=path/to/the/script
     ```

   - 启动定时服务，默认是 每天 00：30 执行;

     `systemctl --user enable --now ZstuFangyiAuto.timer`

   - 使能 systemd --user 服务常驻;

     `loginctl enable-linger ${USER}`

## URL & Port

打卡页面: `http://fangyi.zstu.edu.cn:6006/iForm/1817056F47E744D3B8488B`

授权服务器端口: `4500`

资源服务器端口: `8008`

## 身份认证接口

- Discovery Endpoint

  - URL: `http://fangyi.zstu.edu.cn:4500/.well-known/openid-configuration`

  - 功能：[Discovery Endpoint](https://identityserver4.readthedocs.io/en/latest/endpoints/discovery.html)

  - 方法: `GET`

  - 返回: 

    - 示例

      见`dump/openid-configuration-return.json`

- Authorize Endpoint

  - URL: `http://fangyi.zstu.edu.cn:4500/connect/authorize`

  - 功能: [Authorize Endpoint](https://identityserver4.readthedocs.io/en/latest/endpoints/authorize.html)

  - 方法: `GET`

  - 参数: [Authorize Endpoint](https://identityserver4.readthedocs.io/en/latest/endpoints/authorize.html)

    - 示例

      ```sh
      client_id=INTERNAL00000000CODE
      redirect_uri=http://fangyi.zstu.edu.cn:6006/oidc-callback
      response_type=code
      scope=email profile roles openid iduo.api
      state=924072ae1ba54f29b8af13aeb72f5548
      code_challenge=QoIxhQsM97o-nXywidpyQI9AR1m_P3601EvnZpqm0Ao
      code_challenge_method=S256
      acr_values=idp:Weixin
      * acr_values=idp:Plaform
      response_mode=query
      ```

      

  - 返回: **url**

    - 示例

      ```sh
      http://fangyi.zstu.edu.cn:6006/oidc-callback
      
      code=6015fa4e72688144ba06b9f08fda5c70bfa278a688de38b3f2af341df22761bc
      scope=email profile roles openid iduo.api
      state=924072ae1ba54f29b8af13aeb72f5548
      session_state=_8EwX3YCNs7Q7CQYYlLAtTa-YrwrVFGkZldgvlMap-Q.1a9980aa6a598f98826cbb92a69eeb01
      ```

      

- Token Endpoint

  - URL: `http://fangyi.zstu.edu.cn:4500/connect/token`

  - 功能: [Token Endpoint](https://identityserver4.readthedocs.io/en/latest/endpoints/token.html)

  - 方法: `POST`

  - Header: `Content-Type: application/x-www-form-urlencoded`

  - 数据:  [Token Endpoint](https://identityserver4.readthedocs.io/en/latest/endpoints/token.html)

    - 示例

      ```sh
      client_id=INTERNAL00000000CODE
      client_secret=INTERNAL-b5d5-7eba-1d182998574a
      code=6015fa4e72688144ba06b9f08fda5c70bfa278a688de38b3f2af341df22761bc
      redirect_uri=http://fangyi.zstu.edu.cn:6006/oidc-callback
      code_verifier=748694478cc44ab689d017e6f286a67a7e7e4acaaea849c58eb680b65e6c865c82e109667bb640d3a03d0649a85bc97a
      grant_type=authorization_code
      ```

  - 返回: **json**

    - 示例

      见 `dump/token-return.json`

- UserInfo Endpoint

  - URL: `http://fangyi.zstu.edu.cn:4500/connect/userinfo`

  - 功能: [UserInfo Endpoint](https://identityserver4.readthedocs.io/en/latest/endpoints/userinfo.html)

  - 方法: `GET`

  - Header: `Authorization: Bearer <access_token>`

  - 返回: **json**

    - 示例

      见 `dump/userinfo-return.json`

- CheckSession Endpoint

  - URL: `http://fangyi.zstu.edu.cn:4500/connect/checksession`

  - 功能: 未知

  - 方法: `GET`

  - 返回: **html**

    - 示例

      见 `dump/checksession-return.html`

## 数据接口

- GetDataSourceByNo

  - URL: `http://fangyi.zstu.edu.cn:8008/form/api/DataSource/GetDataSourceByNo`

  - 功能：查询今日健康打卡提交的数据

  - 方法： `GET`

  - 参数：

    - sqlNo: SQL 指令

      | 命令                          | 功能                                   |
      | ----------------------------- | -------------------------------------- |
      | ZJDK_XS$20xxxxxx00001 | 最近打卡_学生 (获取最近一天的表单信息) |
      | JTDK_XS$20xxxxxx00001 | 今天打卡_学生 (请求今天打卡的表单信息)     |
      | SELECT_WXDKRYMX$20xxxxxx00001 | (?)微信打卡人员明细(未知)              |
      | SELECT_ZGFXDQ$上海市浦东新区 | 中国发现地区(判断该地区是不是风险地区) |
      | SELECT_ZGFXDQRY$xxxxxxxxxxx | 中国发现地区人员(检查该人员是不是已经在风险地区人员数据库内) |
      | INSERT_ZGFXDQRY$xxxxxxxxxxx | 保存中国发现地区人员(保存该人员为风险地区人员) |

  - 返回： **json**

    - 示例

      见 `dump/GetDataSourceByNo-return.json`

    

- SubmitBusinessForm

  - URL: `http://fangyi.zstu.edu.cn:8008/form/api/FormHandler/SubmitBusinessForm`

  - 功能： 提交今日健康打卡的数据

  - 方法：`POST`

  - 数据：**json**

    - 示例

      见 `dump/SubmitBusinessForm-post.json`

  - 返回: **json**

    - 示例

    ```json
    {
    	"code": 1,
    	"data": null,
    	"message": "表单提交成功：提交操作“1”条数据",
    	"count": 0
    }
    ```

    

- GetFormInfo

  - URL: `http://fangyi.zstu.edu.cn:8008/form/api/FormHandler/GetFormInfo`
  
  - 功能： 获取今日健康打卡的表格内容

  - 方法：`POST`
  
  - 参数： 
    - formId:	打卡页面的Form ID, 固定为 1817056F47E744D3B8488B
    - bizId:         (Business ID) 未知, 默认为空

  - 数据：**json**

    - 示例
  
    ```json
    {
    	"Sys_UserId": "学号",
    	"Sys_UserName": "姓名",
    	"Sys_UserAccount": "ZSTU/学号",
    	"Sys_CompanyId": null,
    	"Sys_CompanyName": null,
    	"Sys_DepartmentId": "xxxxxxxxxxxxxxxxx",
    	"Sys_DepartmentName": "xx专业xx(x)班级",
    	"Sys_UserEmail": "学号@mails.zstu.edu.cn",
    	"Sys_UserPhone": "",
    	"Sys_JobName": "学生",
    	"Sys_ApplyDate": "yyyy-mm-dd xx:xx:xx",
    	"Sys_OrgPath": "浙江理工大学/学生/xx学院/xx专业/xx(x)班级",
    	"Sys_ApplyNo": "xxxxxxxxxxxxxxxxxxxx",
    	"Sys_UserAvatar": ""
    }
    ```

  - 返回: **json**
  
    - 示例
    
      见 `dump/GetFormInfo-return.json`



- GetFileByte

  - URL: `http://fangyi.zstu.edu.cn:8008/form/api/FormExtend/GetFileByte`
  - 功能：获取健康打卡页面的js代码
  - 方法: `GET`
  - 参数：
    - id:		打卡页面的Form ID, 固定为 1817056F47E744D3B8488B
    - t:          请求时的时间戳
    - jsType: 未知，默认为2

  - 返回： **js**

    - 示例

         见`dump/GetFileByte-return.js`
