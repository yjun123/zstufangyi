#!/bin/env python3
from logging import log
from ZstuFangyiApi import Api
from ZstuFangyiAuthentication import Authentication
import json
from add_account import read_json
from log import logger
from datetime import datetime
import getopt
import sys

CONFIG_FILE_PATH = 'config/accounts.json'

class Fangyi(Api):
    def __init__(self, login: bool= True):
        self.token = None
        if login:
            self.Login()

        super().__init__(self.token)

    def Login(self):
        """ 登录验证
        """
        a = Authentication()
        data = read_json('config/authorize_data.json')
        authorize_data, getcode_data = data['authorize_data'], data['getcode_data']
        # print(getcode_data, authorize_data)
        a.Authorize(authorize_data)
        self.token = a.GetToken(a.GetCode(getcode_data))

    def GetLastCheckinData(self, id):
        """获取最近的打卡记录
        """
        return self.GetDataSourceByNo(f'ZJDK_XS${id}')

    def GetTodayCheckinData(self, id):
        """获取今天的打卡记录
        """
        return self.GetDataSourceByNo(f'JTDK_XS${id}')

    def SubmitTodayCheckinData(self, data):
        """"提交今天的打卡记录
        """

        return self.SubmitBusinessForm(data)

    def GetCheckinForm(self, datafile):
        """"获取今天的打卡页面内容
        """
        with open(datafile) as f:
            report_data = json.load(f)

        return self.GetFormInfo(report_data)

    def UpdateArriveStatus(self, data):
        # 定位不在钱塘区，但是填的 在校; 则更新为不在校
        if data[0]['CURRENTLOCATION'] != '浙江省 杭州市 钱塘区' and data[0]['ARRIVESTATUS'] == '在（入）校':
            logger.warning("检测到定位和在校情况不符合")
            data[0]['ARRIVESTATUS'] = '不在（入）校'

            return (True, data)
        return (False, data)

    def Checkin(self, id, name, force_submit: bool = False):
        """打卡
        """
        # 获取最近的打卡数据
        last_data = self.GetLastCheckinData(id)['data']
        # 获取今天的打卡数据
        today_data = self.GetTodayCheckinData(id)['data']

        # 有打卡记录但是今日未打卡
        if last_data:
            # 根据定位更新是否在校信息
            update_flag, last_data = self.UpdateArriveStatus(last_data)

            if today_data and force_submit == False and update_flag == False:
                logger.warning(f'学号 {id}, 姓名 {name}, 今日已打卡, 打卡取消!!!')
                return

            # 由最近的打卡数据生成今天的打卡数据
            today_data = GetTodayDataFromLastData(last_data[0])
            # ShowPrettyJson(today_data)
        
            ans = self.SubmitTodayCheckinData(today_data)
            # ShowPrettyJson(ans)

            if "表单提交成功" in ans['message']:
                logger.info(f"""学号 {id}, 姓名 {name}, 打卡成功 !""")
            else:
                logger.info(f"""学号 {id}, 姓名 {name}, 打卡失败 !""")
        
        else:
            logger.warning(f"学号 {id}, 姓名 {name} 这位同学没有打卡记录， 打卡失败!!!")


def ShowPrettyJson(data):
    print(json.dumps(data, indent=4, ensure_ascii=False))

def ShowLastCheckinData(id):
    logger.info('获取最近的打卡数据:')
    f = Fangyi()
    data = f.GetLastCheckinData(id)
    ShowPrettyJson(data)
    sys.exit(0)

def ShowTodayCheckinData(id):
    logger.info('获取最近的打卡数据:')
    f = Fangyi()
    data = f.GetTodayCheckinData(id)
    ShowPrettyJson(data)
    sys.exit(0)

def CheckinLoop(force_submit = False):
    f= Fangyi()
    accounts = read_json(CONFIG_FILE_PATH)

    if accounts:
        for u in accounts:
            id = u['username']
            name = u['name']
            active = u['active']

            if active:

                logger.info(f"""
            正在给{name} 打卡 ..........
            """)
            else:
                logger.warning(f"""
            已经禁止{name} 打卡 ..........
            """)
                continue
            # ShowPrettyJson(u)
            # 开始打卡
            f.Checkin(id, name, force_submit)
    else:
        logger.info("""
    没有读取到帐号，请添加...
    """)

def CheckinOne(id, name = 'NULL', force_submit = False):
    f= Fangyi()


    logger.info(f"""
        正在给{id} 打卡 ..........
        """)
        
    # 开始打卡
    f.Checkin(id, name, force_submit)

def GetTodayDataFromLastData(l_data):
    t_data = read_json('config/upload-template.json')
    t_data["biz"]['CURRENTDATE'] = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    t_data["biz"]['IDCODE'] = l_data['IDCODE']
    t_data["biz"]['CURRENTTIME'] = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    t_data["biz"]['NAME'] = l_data['NAME']
    t_data["biz"]['DEPARTMENT'] = l_data['DEPARTMENT']
    t_data["biz"]['CURRENTLOCATION'] = l_data['CURRENTLOCATION']
    t_data["biz"]['CURRENTSITUATION'] = l_data['CURRENTSITUATION']
    t_data["biz"]['ARRIVESTATUS'] = l_data['ARRIVESTATUS']
    t_data["biz"]['TEMPERATURESITUATION'] = l_data['TEMPERATURESITUATION']
    t_data["biz"]['HEALTHCODESTATUS'] = l_data['HEALTHCODESTATUS']
    t_data["biz"]['VACCINATIONSTATUS'] = l_data['VACCINATIONSTATUS']
    t_data["biz"]['CONFIRMEDSTATE'] = l_data['CONFIRMEDSTATE']
    t_data["biz"]['QUARANTINESTATUS'] = l_data['QUARANTINESTATUS']
    t_data["biz"]['CLR'] = l_data['CLR']
    t_data["biz"]['XGNR'] = l_data['XGNR']
    t_data["biz"]['LIVINGHISTORYSTATUS'] = l_data['LIVINGHISTORYSTATUS']
    t_data["biz"]['DKLX'] = l_data['DKLX']
    t_data["biz"]['TZRY'] = l_data['TZRY']
    t_data["biz"]['ZHJZSJ'] = l_data['ZHJZSJ']
    t_data["biz"]['XGYMZL'] = l_data['XGYMZL']
    t_data["biz"]['TZRYSM'] = l_data['TZRYSM']
    t_data["biz"]['SFYHSYXBG'] = l_data['SFYHSYXBG']
    t_data["biz"]['HSJCSJ'] = l_data['HSJCSJ']
    t_data["biz"]['HSJCDD'] = l_data['HSJCDD']
    t_data["biz"]['KYJCJG'] = l_data['KYJCJG']
    t_data["biz"]['DQXXZT'] = l_data['DQXXZT']
    t_data["user"]['userName'] = l_data['NAME']
    t_data["user"]['userId'] = f'ZSTU/{l_data["IDCODE"]}'
    
    return t_data

def main(argv):

    username = None
    submit_flag = False
    force_flag = False
    last_flag = False

    try:
        opts, args = getopt.getopt(argv, "u:sfl", ["username=", "submit", "force", "last"])
    except getopt.GetoptError:
        help()
        sys.exit(-1)
    
    for opt, arg in opts:
        if opt == '-h':
            help()
            sys.exit(0)
        elif opt in ('-u', "--username"):
            username = arg
        elif opt in ('-s', "--submit"):
            submit_flag = True
        elif opt in ('-f', "--force"):
            force_flag = True
        elif opt in ('-l', "--last"):
            last_flag = True

    if username and submit_flag:
        if force_flag:
            logger.info("检测到 学号, 提交 和强制提交")
            CheckinOne(username, 'NULL',True)
        else:
            logger.info("检测到 学号, 提交")
            CheckinOne(username)

        sys.exit(0)

    if username :
        if last_flag:
            logger.info("检测到 学号和 查看最近记录")
            ShowLastCheckinData(username)
        else:
            logger.info("检测到 学号和 查看今天记录")
            ShowTodayCheckinData(username)

    if submit_flag:
        logger.info("检测到 提交")
        CheckinLoop()

if __name__ == '__main__':
    main(sys.argv[1:])
