import requests
from log import logger
import json
import urllib.parse

class Authentication():
    def __init__(
        self,
        host: str = 'fangyi.zstu.edu.cn',
        port: int = 4500,
        timeout: int = 10
    ):
        self.host = host
        self.port = port
        self.timeout = timeout
        self.client = requests.Session()

        # resp = self.get(
        #     '.well-known/openid-configuration'
        # )

        # logger.info(f"{json.dumps(resp, indent=4, ensure_ascii=False)}")

    def GetPublicKey(
        self
    ):
        """ 获取的是公钥，用于验证jwt的数字签名部分
        """
        return self.get(
            "openid-configuration/jwks"
        )
    
    def GetCode(
        self,
        data
    ):
        r = self.client.post(
            'https://sso.zstu.edu.cn/login',
            data= data
        )    

        url = r.history [-1].headers["location"]
        params = urllib.parse.urlparse(url).query
        code = urllib.parse.parse_qs(params)['code'][0]
        return code

    def Authorize(
        self,
        data
    ):
        """ 授权服务器的授权端点
        """
        return self.get(
            "connect/authorize",
            params= data
        )

    def GetToken(
        self,
        code: str,
        client_id: str = 'INTERNAL00000000CODE',
        client_secret: str = 'INTERNAL-b5d5-7eba-1d182998574a',
        redirect_uri: str = 'http://fangyi.zstu.edu.cn:6006/oidc-callback',
        code_verifier: str = '748694478cc44ab689d017e6f286a67a7e7e4acaaea849c58eb680b65e6c865c82e109667bb640d3a03d0649a85bc97a',
        grant_type: str = 'authorization_code'    
    ):
        """ 获取token (access_token)
        """
        # 设置 Content_Type 为 application/x-www-form-urlencoded 是必须的， 否则会造成 400
        # https://identityserver4.readthedocs.io/en/latest/endpoints/token.html#example

        headers = { 'Content-Type': 'application/x-www-form-urlencoded'}

        return self.post(
            "connect/token",
            payload= {
                "client_id" : client_id,
                "client_secret" : client_secret,
                "code" : code,
                "redirect_uri" : redirect_uri,
                "code_verifier" : code_verifier,
                "grant_type" : grant_type
            },
            headers= headers
        )['access_token']

    def GetUserInfo(
        self,
        token: str
    ):
        """ 根据token获取用户信息
        """

        # 设置 Authorization 为 Bearer token 和 设置 Content-Type 为 application/json 是必须的
        # https://identityserver4.readthedocs.io/en/latest/endpoints/userinfo.html#example

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}',
        }

        return self.get(
            "connect/userinfo",
            headers= headers
        )

    def CheckSession(
        self
    ):
        """ 客户端对check_session_iframe执行监视，可以获取用户的登出状态
        """
        return self.get(
            "connect/checksession",
        )

    #######################
    def baseRequest(
        self,
        method: str,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None
    ) -> dict:
        """基础请求方法"""

        if params:
            params = urllib.parse.urlencode(params, quote_via=urllib.parse.quote)
        try:
            resp = self.client.request(
                method,
                f'http://{self.host}:{self.port}/{path}',
                headers= headers,
                params= params,
                data = payload,
                timeout= self.timeout
            )
            resp.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if isinstance(e, requests.exceptions.Timeout):
                logger.warning(f'响应超时! => {e}')
            elif isinstance(e, requests.exceptions.HTTPError):
                logger.error(f'响应码出错 => {resp.status_code}')
            else:
                logger.error(f'请求出错')
            
            print(resp.text)
            return {}

        # 处理数据
        try:
            data = resp.json()
        except json.JSONDecodeError as e:
            logger.error('响应结果非json格式')
            return {}
        
        if data is None:
            logger.error('返回为 NULL')
            return {}

        return data

    def post(
        self,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None,
    ):
        return self.baseRequest(
            'POST',
            path,
            params= params, payload= payload, headers= headers
        )  

    def get(
        self,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None
    ):
        return self.baseRequest(
            'GET',
            path,
            params= params, payload= payload, headers= headers
        )

import sys
if __name__ == '__main__':
    a = Authentication()
    a.Authorize()
    token = a.GetToken(a.GetCode())
    print(json.dumps(a.GetUserInfo(token), indent=4, ensure_ascii=False))
    # print(json.dumps(a.CheckSession() , indent=4, ensure_ascii=False))
