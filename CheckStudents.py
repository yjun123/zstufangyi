from os import read
from ZstuFangyi import Fangyi
from add_account import read_json

CONFIG_FILE_PATH='config/accounts.json'

def main():
    F = Fangyi()

    accounts = read_json(CONFIG_FILE_PATH)

    for u in accounts:
        username = u['username']
        name = u['name']

        today_data = F.GetDataSourceByNo(f'JTDK_XS${username}')['data']

        if today_data:
            dates = [i['CURRENTDATE'] for i in today_data]
            ZHJZSJ = today_data[0]['ZHJZSJ']
            print(f"""
    **********************
    学号{username} 今日已打卡 {len(today_data)} 次

    姓名: {name}
    打卡时间: {dates}
    最近接种时间: {ZHJZSJ}
    ***********************""")
        else:
            print(f"""
    ！！！！！！！！！！！！！！
    学号{username} 今日未打卡

    姓名: {name}
    ！！！！！！！！！！！！！！
            """)

if __name__ == '__main__':
    main()
